## Overview

`dataops-pqrs` is a comprehensive project designed to simulate, control, analyze, and visualize the processing of PQRS (Public Query and Response System) data. This project is structured into several stages, each focusing on different aspects of data operations, including simulation, internal control, data analysis, business intelligence, and dashboard creation. It also includes robust data pipelines for data cleansing and summarization.

## Requirements

Install the required Python packages using:

```bash
pip install -r requirements.txt
```

## Instructions

Before running the Makefile for each stage, you must first execute the Makefile in the `data_lake` folder. This ensures that all necessary data is prepared and available for the subsequent stages.

### Steps

1. Navigate to the `data_lake` folder and execute the Makefile:

    ```bash
    cd data_lake
    make
    ```

2. After executing the Makefile in the `data_lake` folder, you can proceed to run the Makefile for each stage as needed. Below are the instructions for each stage:

### Stage 1: Simulator

Navigate to the `stage-1-simulator` folder and execute the Makefile:

```bash
cd ../stage-1-simulator
make
```

### Stage 2: Internal Control

Navigate to the `stage-2-internal-control` folder and execute the Makefile:

```bash
cd ../stage-2-internal-control
make
```

### Stage 3: Data Analysis

Navigate to the `stage-3-data-analysis` folder and execute the Makefile:

```bash
cd ../stage-3-data-analysis
make
```

### Stage 4: Business Intelligence

Navigate to the `stage-4-business-intelligence` folder and execute the Jupyter Notebook `bi_report.ipynb`:

```bash
cd ../stage-4-business-intelligence
jupyter notebook bi_report.ipynb
```

### Stage 5: Dashboard

Navigate to the `stage-5-dashboard` folder and execute the Makefile:

```bash
cd ../stage-5-dashboard
make
```

### Stage 6: Data Pipelines

Navigate to the `stage-6-data-pipelines` folder and execute the Makefile in the following order:

```bash
cd ../stage-6-data-pipelines
make full_ingest
make incremental_ingest
make data_analysis_report
```

## Stage 1: Simulator

The simulator is responsible for generating and processing PQRS data. It simulates the workflow of requests being opened, assigned, and closed over time.

### Files

- `advance.py`: Advances the simulation by a specified number of weeks.
- `restart.py`: Restarts the simulation from a specific date.
- `simulator.py`: Contains the core logic for processing and simulating PQRS requests.
- `historical_requests_table.csv`: Contains historical request data for the simulation.

### Usage

Advance the simulation by one week:

```bash
python stage-1-simulator/advance.py
```

Advance the simulation by a specific number of weeks:

```bash
python stage-1-simulator/advance.py <number_of_weeks>
```

Restart the simulation from a specific date:

```bash
python stage-1-simulator/restart.py <restart_date>
```

Restart the simulation from the default date:

```bash
python stage-1-simulator/restart.py
```

## Stage 2: Internal Control

Generates internal control reports to ensure that requests are being processed within acceptable time limits.

### Files

- `internal_control_report.py`: Computes delays in request processing and generates reports.

### Usage

Generate an internal control report:

```bash
python stage-2-internal-control/internal_control_report.py
```

## Stage 3: Data Analysis

Performs data analysis on the PQRS data to generate various reports.

### Files

- `data_analysis_report.py`: Analyzes open requests and generates status frequency and days elapsed reports.

### Usage

Generate data analysis reports:

```bash
python stage-3-data-analysis/data_analysis_report.py
```

## Stage 4: Business Intelligence

Contains a Jupyter notebook for in-depth business intelligence analysis.

### Files

- `bi_report.ipynb`: Notebook for BI analysis.

## Stage 5: Dashboard

Provides a web-based dashboard to visualize the PQRS data.

### Files

- `app.py`: Flask application for the dashboard.
- `hello.py`: Simple Flask app for testing.
- `make_plot.py`: Generates plots for the dashboard.
- `static/report.png`: Static images for the dashboard.
- `templates/index.html`: HTML template for the dashboard.

### Usage

Run the dashboard:

```bash
python stage-5-dashboard/app.py
```
## Stage 6: Data Pipelines

Manages data ingestion, cleansing, and summarization pipelines.

### Files

- `config`: Contains YAML configuration files for various pipelines.
- `source`: Contains the scripts for data pipelines.

### Usage

Run the full data copy pipeline:

```bash
python stage-6-data-pipelines/source/full_copy_from_csv.py stage-6-data-pipelines/config/ingest_rdbms_requests_table.yaml
```

Run the incremental data copy pipeline:

```bash
python stage-6-data-pipelines/source/incremental_copy_from_csv.py stage-6-data-pipelines/config/ingest_rdbms_requests_table.yaml
```

Run the data cleansing pipeline:

```bash
python stage-6-data-pipelines/source/make_cleansing.py stage-6-data-pipelines/config/cleansing_pqrs_web.yaml
```

Run the data summarization pipeline:

```bash
python stage-6-data-pipelines/source/summarize.py stage-6-data-pipelines/config/summarize_pqrs.yaml
```